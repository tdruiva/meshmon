# MeshMon - Monitoramento para redes em malha

## Objetivos

- Monitorar nodos de rede utilizando o collectd do openwwrt
- Utilizar Influxdb para TSDB e Grafana para GFE
- Utilizar Github para arquivos e configs
- Utilizar docker para automatizar instalação
- Provisionar o máximo possível


## Pre requisitos:

- hardware pc ou rasp.
	- sistema operacional linux instalado
	- git, docker e docker-compose instalado
- roteador
	- com openwrt e luci-app-statistics ou collectd instalado
	- configurar collectd para enviar os dados ao ip do servidor onde será instalado o monitor. **Importante configurar antes de testar o ambiente gráfico!**

### passo a passo

Depois de instalar `docker`, `docker-compose` e `git` na máquina que será o monitor (testados em PC e Raspberry Pi 3 e 4, com Debian Gnu Linux e Arch Linux):

Clonar este repositório:

```
git clone https://gitlab.com/rtroian/meshmon.git
```

Entrar no diretório e alterar o valor de `url` em `datasource.yaml`.

```
cd meshmon
vi configs/grafana/datasource.yaml

```
Mude `url: http://10.5.10.5:8086` para o ip ou nome de onde será instalado o monitor e salve o arquivo.


Execute o docker-compose:

```
docker-compose up -d
```

Se tudo deu certo, você deverá acessar Grafana em um navegador na rede, através do endereço `http://ipdoservidor:3000`.
Usuário e senha padrão é: `admin`

Em caso de problemas, para manter os logs em tela durante os testes usaremos o modo up e sem -d (desatachar)

```
docker-compose up
```

Se aparecer um erro como esse:

	ERROR: Couldn't connect to Docker daemon at http+docker://localunixsocket - is it running?

É provavel que o seu usuário não tenha permissão para rodar o docker. Para isso é neceessário adicionar o nome de seu usuario ao grupo docker em /etc/group e deslogar/relogar para as alterações fazerem efeito.

Outros erros podem ser identificados pelas mensagens presentes nos logs.



## Caracteristicas técnicas
### Influxdb

Time Series Database

versão 1.7.10

Receber dados dos nodos por Collectd
porta 25826

Servir dados ao Grafana
porta 8086

Armazenar dados em pastas acessíveis pelo host

Provisionar:
- Arquivo de config com ajustes collectd influxdb.conf
- arquivo com dados estatísticas openwrt types.db
- config collectd sem senha

### Grafana

Graphics Front End

grafana/grafana:master-ubuntu

Coletar dados no influxdb
porta 8086

Expor dados por gráficos em
porta padrao 3000

Manter senha padrão inicialmente
admin admin

Armazenar dados em pastas acessíveis pelo host

Provisionar:
- Arquivo de config com datasource (coolectd)
- Arquivo config dashboard yml
- arquivo dashboard json


